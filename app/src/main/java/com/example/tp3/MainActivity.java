package com.example.tp3;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;


import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.View;

import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.tp3.SportDbHelper.COLUMN_LEAGUE_NAME;
import static com.example.tp3.SportDbHelper.COLUMN_TEAM_NAME;
import static com.example.tp3.SportDbHelper._ID;


public class MainActivity extends AppCompatActivity {
    private EditText textTeam;

    TextView textview1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textTeam = (EditText) findViewById(R.id.editNewName);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textview1 = findViewById(R.id.text1);
        ListView listView = findViewById(R.id.list_view);
        SportDbHelper dbHelper = new SportDbHelper(this);
        Cursor c =  dbHelper.fetchAllTeams();

        dbHelper.populate();
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                c,
                new String[]{SportDbHelper.COLUMN_TEAM_NAME, SportDbHelper.COLUMN_LEAGUE_NAME},
                new int[]{android.R.id.text1, android.R.id.text2});
             listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {
                // Ton traitement souhaité lors du click,
                // la variable position est en fait l'indice de l'item cliqué
                //à partir de là tu  peux récupérer l'objet souhaité de ton tableau


                final Intent intent = new Intent(MainActivity.this,TeamActivity.class);
                //intent.putExtra(Team.TAG, t);



                startActivity(intent);

            }
        });

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
        */


    }
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1)
        {

                String message = data.getStringExtra("MESSAGE");// etc.}
                textview1.setText(message);

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
